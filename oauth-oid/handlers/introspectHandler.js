import AccessTokenType from '../../../../token-types/AccessTokenType';
import RefreshTokenType from '../../../../token-types/RefreshTokenType';
import IdTokenType from '../../../../token-types/IdTokenType';
import createClientKey from '../../../../token-types/createClientKey';

const { query, dispatch } = require('nact');

export default async function (args, ctx) {
  const { token } = args;
  const { token_type_hint } = args;

  switch (token_type_hint) {
    case 'refresh_token':
      return new RefreshTokenType(token);
      break;
    case 'access_token':
      const serverConfigActor = ctx.children.get('server_config');
      const config = await query(serverConfigActor, {}, 20000);
      const key = await createClientKey(config);
      const access_token = new AccessTokenType(token, key);
      await access_token.decrypt();
      return access_token.getPayloadJSON();
      break;
    case 'id_token':
      return new IdTokenType(token);
      break;
    default:
      break;
  }
}
