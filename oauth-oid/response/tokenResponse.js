export default async function (access_token, expires_in, refresh_token, scope, id_token) {
  return {
    token_type: 'Bearer',
    access_token,
    expires_in,
    refresh_token,
    scope,
    id_token,
  };
}

//    bearer-jwt
//    token_type: String!
//    access_token: String!
//    expires_in: String
//    refresh_token: String
//    id_token: String
//    scope: [String]
